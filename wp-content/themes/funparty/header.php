<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="icon" type="image/png" sizes="32x32" href="favicon.ico">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?> id="home">

<header class="site-header">
    <div class="row">
        <div class="small-12 large-2 columns">
            <h1 class="site-title"><?php echo get_bloginfo('description'); ?></h1>
            <a href="#home" class="logo" title="<?php echo get_bloginfo('title'); ?>">
                <img src="<?php echo get_bloginfo('template_url') ?>/assets/images/logo-fun.png" alt="<?php echo get_bloginfo('title'); ?>" />
            </a>
        </div>

        <div class="show-for-large large-10 columns">
            <nav class="site-navigation">
                <?php
                    $args = array(
                        'theme_location' => 'primary'
                    );
                ?>

                <?php wp_nav_menu( $args ); ?>
            </nav>
        </div>
    </div>

    <a href="javascript:void(0);" class="mobile-menu toggle-menu hide-for-large"><i class="fas fa-bars"></i></a>

    <div class="offset-menu hide-for-large">
        <a href="javascript:void(0);" class="mobile-menu-close toggle-menu"><i class="fas fa-times"></i></a>
        <?php wp_nav_menu( $args ); ?>
    </div>
</header>
    