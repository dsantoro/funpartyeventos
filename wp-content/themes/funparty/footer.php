<footer class="site-footer">
    <div class="row">
        <div class="small-12 medium-6 columns">
            <a href="#home" class="logo" title="<?php echo get_bloginfo('title'); ?>">
                <img src="<?php echo get_bloginfo('template_url') ?>/assets/images/logo-fun.png" alt="<?php echo get_bloginfo('title'); ?>" />
            </a>
        </div>
        <div class="small-12 medium-6 columns">
            <ul class="socials">
                <li>Estamos nas seguintes redes sociais: </li>
                <?php while(have_rows('redes_sociais', 39)) : the_row()?>
                    <li><a href="<?php the_sub_field('redes_sociais_link'); ?>" target="_blank"><i class="<?php the_sub_field('redes_sociais_icone'); ?>"></i></a></li>
                <?php endwhile ?>
            </ul>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>

</body>
</html>