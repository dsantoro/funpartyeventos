(function ($) {
    $(document).ready(function () {
        console.log("We're ready");

        $('body').on('click', function () {
            $('.wpcf7-response-output').fadeOut();
        });

        $(window).on('scroll', function () {
            var scroll = window.scrollY;
            
            if(scroll > 10) {
                $('.site-header').addClass('scrolled');
            } else {
                $('.site-header').removeClass('scrolled');
            }
        });

        $('.parallax').each(function(){
            var obj = $(this);
        
            $(window).scroll(function() {
                var yPos = -($(window).scrollTop() / obj.data('speed')); 
        
                var bgpos = '50% '+ yPos + 'px';
        
                obj.css('background-position', bgpos );
            }); 
        });

        $('.toggle-menu').on('click', function() {
            $('body').toggleClass('menu-opened');
        });

        $(document).on('click', 'a[href^="#"]', function (e) {
            e.preventDefault();
            $('html, body').stop().animate({
                scrollTop: $($(this).attr('href')).offset().top
            }, 1000, 'linear');
        });
    });
})(jQuery);
