<?php

    /*
        Função que faz o carregamento dos recursos (resources) do tema
    */
    function resources() {
        wp_enqueue_style('foundation', get_template_directory_uri() . '/assets/css/foundation.min.css', false, '1.1', 'all');

        wp_enqueue_style('fonts', get_template_directory_uri() . '/assets/fonts/fonts.css', false, '1.1', 'all');

        wp_enqueue_style('css-slider', get_template_directory_uri() . '/assets/css/slick.css', false, '1.1', 'all');

        wp_enqueue_style('font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.min.css', true, '5.0', 'all');

        // wp_enqueue_style('css-slider-theme', get_template_directory_uri() . '/assets/css/slick-theme.css', false, '1.1', 'all');

        wp_enqueue_style('fancybox-css', get_template_directory_uri() . '/assets/css/jquery.fancybox.min.css', false, '1.1', 'all');

        wp_enqueue_style('app', get_template_directory_uri() . '/assets/css/app.min.css', false, '1.1', 'all');

        wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', array(), '2.2', true);

        wp_enqueue_script('slick', get_template_directory_uri() . '/assets/js/slick.min.js', array(), '1.8', true);

        wp_enqueue_script('fancybox', get_template_directory_uri() . '/assets/js/jquery.fancybox.min.js', array(), '3', true);

        wp_enqueue_script('app', get_template_directory_uri() . '/assets/js/app.js', array(), '1.1', true);
    }

    /*
        Chamada da função que faz o carregamento dos recursos (resources) do tema
    */
    add_action('wp_enqueue_scripts', 'resources');

    /*

        Registros de Menu
    */
    register_nav_menus(array(
        'primary' => __('Primary Menu'),
        'childrens_menu' => __('Childrens Menu'),
    ));

    // Get top ancestor
    function get_top_ancestor_id() {
        global $post;
        
        if ($post->$post_parent) {
            $ancestors = array_reverse(get_post_ancestors($post->ID));
            return $ancestors[2];
        }

        return $post->ID;
    }

    function has_children() {
        global $post;

        $pages  = get_pages('child_of=' . $post->ID);

        return count($pages);
    }

    function add_custom_class($classes=array(), $menu_item=false) {
        if ( !is_page() && 'Blog' == $menu_item->title && 
                !in_array( 'current-menu-item', $classes ) ) {
            $classes[] = 'current-menu-item';        
        }                    
        return $classes;
    }
    add_filter('nav_menu_css_class', 'add_custom_class', 100, 2); 

    // Remove trash tags do snippet gerado do CF7
    add_filter('wpcf7_form_elements', function($content) {
        $content = preg_replace('/<(span).*?class="\s*(?:.*\s)?wpcf7-form-control-wrap(?:\s[^"]+)?\s*"[^\>]*>(.*)<\/\1>/i', '\2', $content);

        return $content;
    });

    set_post_thumbnail_size( 400, 400, true );

    add_theme_support('post-formats', array('aside', 'gallery', 'link'));

    function theme_features() {
        add_theme_support('title-tag');
        add_theme_support( 'post-thumbnails', array( 'post' ) );
    }

    add_action('after_setup_theme', 'theme_features');