<?php
    get_header();
?>

<section class="banner" role="banner" style="background-image: url('<?php echo get_bloginfo('template_url') ?>/assets/images/banner.jpg')">
    <div class="row">
        <div class="small-12 columns">
            <h2><?php the_field('titulo'); ?></h2>
            <h3><?php the_field('sub_titulo'); ?></h3>

            <div class="center-mobile">
                <a href="#orcamento" class="btn-default">Solicitar Orçamento</a>
            </div>
        </div>
    </div>
</section>

<section class="perks">
    <div class="small-up-2 medium-up-4 row">
        <?php while(have_rows('beneficios')) : the_row()?>
            <div class="column column-block">
                <div class="perks__item">
                    <i class="<?php the_sub_field('beneficios_icone'); ?>"></i>
                    <h4><?php the_sub_field('beneficios_titulo'); ?></h4>
                    <?php the_sub_field('beneficios_texto'); ?>
                </div>
            </div>
        <?php endwhile ?>
    </div>
</section>

<section class="celebrate parallax" data-speed="5">
    <div class="row">
        <div class="small-12 medium-10 medium-centered large-8 large-centered columns">
            <h2><?php the_field('celebre_titulo'); ?></h2>

            <?php the_field('celebre_texto'); ?>

            <a href="#orcamento" class="btn-default">Solicite um orçamento</a>
        </div>
    </div>
</section>

<section class="our-services">
    <div class="row">
        <div class="small-12 columns">
            <h4>Nossos Serviços</h4>

            <div class="grid-template">

                <?php while(have_rows('galeria', 46)) : the_row()?>
                    <a data-fancybox="gallery" href="<?php the_sub_field('imagem'); ?>" class="grid-template__item" title="<?php the_sub_field('imagem_texto'); ?>">
                        <img src="<?php the_sub_field('imagem'); ?>" alt="<?php the_sub_field('imagem_texto'); ?>" />
                    </a>
                <?php endwhile ?>
            </div>
        </div>
    </div>
</section>

<section class="cta">
    <div class="row">
        <div class="small-12 columns">
            <div class="cta__container">
                <hgroup>
                    <h3>Procurando por algo realmente especial?</h3>
                    <h4>Entre em contato e faça um orçamento!</h4>
                </hgroup>

                <a href="#orcamento" class="btn-default">Solicite um orçamento</a>
            </div>
        </div>
    </div>
</section>

<section class="contact" id="orcamento">
    <div class="row">
        <div class="small-12 medium-8 medium-centered large-6 large-centered columns">
            <h6 class="text-center">Vamos começar!</h6>

            <p>Preencha os dados abaixo e ligamos para você para começarmos com seu orçamento.</p>

            <?php echo do_shortcode('[contact-form-7 id="16" title="Formulário de contato 1"]') ?>
        </div>
    </div>
</section>

<?php 
    get_footer();
?>